package com.techu.apitechudb.services;


import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {



    @Autowired
    ProductRepository productRepository;

    public List<ProductModel> findAll() {
        System.out.println("find en ProductService");
        return this.productRepository.findAll();
    }
//optional se utiliza para manejar los nulos cuando algo no esta informado , en lugar de poner Ifs anidados
//esta pensado para ser un tipo de retorno de las funciones
// el optioal te permite mirar dentro de la caja productmodel, puede contener datos o estar vacia.
// no te saltara el null exception, y puedes trabajar con la caja.


    public Optional<ProductModel> findById(String id){
           System.out.println("find by Id en ProductService");

           return this.productRepository.findById(id);
    }

    public ProductModel addProduct(ProductModel product){
        System.out.println("addProduct en product service");

        return this.productRepository.save(product);
    }

    public ProductModel update(ProductModel product) {
        System.out.println("Update en product service");

        return this.productRepository.save(product);
    }

    public boolean delete(String id) {
        System.out.println("delete en product service");
        boolean result = false;

        if (this.findById(id).isPresent() == true){
            System.out.println("producto encontrado,borrando");
            this.productRepository.deleteById(id);
            result = true;
        }
        return result;

    }
}
