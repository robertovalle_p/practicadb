package com.techu.apitechudb.services;


import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.ProductRepository;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> getUsers(String orderBy) {
        System.out.println("find en UserService");

        List<UserModel> result;

        if (orderBy != null) {
            System.out.println("Se ha pedido orednacion");
            result = this.userRepository.findAll(Sort.by("age"));
        } else {
            System.out.println("no se ha pedido ordenmacion");
            result = this.userRepository.findAll();

        }

        return result;
    }

    public Optional<UserModel> findById(String id) {
        System.out.println("find by Id en UserService");

        return this.userRepository.findById(id);
    }

    public UserModel addUser(UserModel user) {
        System.out.println("addProduct en user  service");

        return this.userRepository.save(user);
    }

    public UserModel update(UserModel user) {
        System.out.println("Update en user  service");

        return this.userRepository.save(user);
    }

    public boolean delete(String id) {
        System.out.println("delete en user service");
        boolean result = false;

        if (this.findById(id).isPresent() == true) {
            System.out.println("usuario encontrado,borrando");
            this.userRepository.deleteById(id);
            result = true;
        }
        return result;


    }
}
