package com.techu.apitechudb.Controllers;


import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.ProductService;
import com.techu.apitechudb.services.UserService;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(
            @RequestParam (name ="$orderby", required = false) String orderBy
    ) {
        System.out.println("getUsers");
        System.out.println("El valor de $orderby es" + orderBy);
        return new ResponseEntity<>(
                this.userService.getUsers(orderBy),
                HttpStatus.OK
        );

    }

        @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id) {
        System.out.println("get userById");
        System.out.println("La id del usuario  a buscar  es " + id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );

    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user) {
        System.out.println("addUser");
        System.out.println("La id del usuario a crear es " + user.getId());
        System.out.println("El nombre del usuarioa crear es " + user.getName());
        System.out.println("La Edad del usuario es a crear es " + user.getAge());

        return new ResponseEntity<>(this.userService.addUser(user), HttpStatus.CREATED);
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel user, @PathVariable String  id){

        System.out.println("UpdateUser");
        System.out.println("La id del usuario  a actualizar  en parametro URL " + id);
        System.out.println("La id   usuario a actualizar es " + user.getId());
        System.out.println("El nombre del  usuario  es a actualizar  es " + user.getName());
        System.out.println("La edad   del usuario   a actualizar es " + user.getAge());

        Optional<UserModel>userToUpdate = this.userService.findById(id);

        if (userToUpdate.isPresent()){
            System.out.println("User para actualizar, actualizando ");
            this.userService.update(user);
        }
        return new ResponseEntity<>(user,
                userToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id ) {
        System.out.println("DeleteteUser");
        System.out.println("La id del usuario a borrar es " + id);

        boolean deletedUser = this.userService.delete(id);

        return new ResponseEntity<>(
                deletedUser ? "Usuario borrado" : "Usuario no borrado",
                deletedUser ? HttpStatus.OK : HttpStatus.NOT_FOUND

        );
    }
}
