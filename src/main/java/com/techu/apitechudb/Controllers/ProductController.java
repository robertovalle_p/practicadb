package com.techu.apitechudb.Controllers;

import com.sun.org.apache.bcel.internal.generic.I2D;
import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts() {
        System.out.println("get products");

        return new ResponseEntity<>(this.productService.findAll(), HttpStatus.OK);

    }
    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id) {
        System.out.println("get productById");
        System.out.println("La id del producto  a buscar  es " + id);

        Optional<ProductModel> result = this.productService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Product no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );

    }


    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product) {
        System.out.println("addProduct");
        System.out.println("La id del producto es a crear es " + product.getId());
        System.out.println("La descripcion  producto es a crear es " + product.getDesc());
        System.out.println("El precio  producto es a crear es " + product.getPrice());

        return new ResponseEntity<>(this.productService.addProduct(product), HttpStatus.CREATED);
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updateProduct(@RequestBody ProductModel product, @PathVariable String  id){

        System.out.println("UpdateProduct");
        System.out.println("La id del producto  a actualizar   es " + id);
        System.out.println("La descripcion  producto es a crear es " + product.getId());
        System.out.println("La descripcion  producto es a crear es " + product.getDesc());
        System.out.println("La Precio  producto es a crear es " + product.getPrice());

        Optional<ProductModel>productToUpdate = this.productService.findById(id);

        if (productToUpdate.isPresent()){
            System.out.println("Producto para actualizar, actualizando ");
            this.productService.update(product);
        }

        return new ResponseEntity<>(product,
                productToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }
    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id ) {
        System.out.println("UpdateProduct");
        System.out.println("La id del producto a borrar es " + id);

        boolean deletedProduct = this.productService.delete(id);

        return new ResponseEntity<>(
                deletedProduct ? "Producto borrado" : "Producto no borrado",
                deletedProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND

        );

    }
}
